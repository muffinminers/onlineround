
public class Ride {
	public int id;
	public int startRow; //a
	public int startCol; //b
	public int endRow; //x
	public int endCol; //y
	public long minStartTime;
	public long maxEndTime;
	public long realStartTime;
	public long realEndTime;
	public boolean rideDone;
	
	
	public Ride(int id, int a, int b, int x, int y, long s, long f) {
		this.id = id;
		startRow = a;
		startCol = b;
		endRow = x;
		endCol = y;
		minStartTime = s;
		maxEndTime = f;
		rideDone = false;
	}

	public long distance() {
		return Math.abs(startRow-endRow)+Math.abs(startCol-endCol);
	}
	
	public long latestStartTime() {
		return maxEndTime-distance()-1;
	}

	public Intersection getEndIntersection() {
		return new Intersection(endRow, endCol);
	}
	
	public Intersection getStartIntersection() {
		return new Intersection(startRow, startCol);
	}

	public long weightAtTime(long t, int distRiders){
		return minStartTime - t + distRiders;
	}

	public void finishedRide(){
		rideDone = true;
	}
}
