import java.util.ArrayList;

public class Function2 {

	public static void assignRides(Worker worker) {
		int rIdx = 0;
		for(int i = 0, cnt = worker.carList.size(), cntR = worker.ridesList.size(); i < cnt && rIdx < cntR; i++) {
			worker.carList.get(i).addRide(worker.ridesList.get(rIdx++));
			if (i + 1 >= cnt) {
				i = -1;
			}
		}
	}
	
	public static void assignRidesV2(Worker worker) {
		for(int i = 0, cntR = worker.rides; i < cntR; i++) {
			Ride ride = worker.ridesList.get(i);
			Car bestCar = worker.carList.get(0);
			long bestScore = bestCar.getScoreForRide(ride, worker.bonus);
			for (int j = 1, cntC = worker.cars; j < cntC; j++) {
				Car car = worker.carList.get(j);
				long score = car.getScoreForRide(ride, worker.bonus);
				if (score > bestScore) {
					bestCar = car;
					bestScore = score;
				}
			}
			bestCar.addRide(ride);
		}
		
	}

	public static void assignRidesV3(Worker worker){
		for(int t = 0; t < worker.maxTime; t++){
			//set free cars
			ArrayList<Car> freeCars = new ArrayList<>();
			for(int i = 0; i < worker.carList.size(); i++){
				if(worker.carList.get(i).free)
					freeCars.add(worker.carList.get(i));
			}
			//set rides to be done
			ArrayList<Ride> rides = new ArrayList<>();
			for(int r = 0; r < worker.ridesList.size(); r++){
				if(!worker.ridesList.get(r).rideDone)
					rides.add(worker.ridesList.get(r));
			}
			//compare weights for each car and sort
			for(int j = 0; j < freeCars.size(); j++){
				freeCars.get(j).compareWeightsAtTime(t, rides);
			}
			perfectAssignment(freeCars);
		}
	}

	private static void perfectAssignment(ArrayList<Car> freeCars) {
		ArrayList<Car> newCars = new ArrayList<>();
		for (int c = 0; c < freeCars.size(); c++) {
			Ride ride = freeCars.get(c).findZeroWeight();
			if (!ride.rideDone) {
				freeCars.get(c).addRide(ride);
				ride.finishedRide();
				freeCars.get(c).setCarFree(false);
			}
		}
	}

}
