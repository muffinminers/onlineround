import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;

public class Car {
	public int id;
	public ArrayList<Ride> rides;
	boolean free;

	public TreeMap<Long, Ride> rideWeightsAtTime;
	
	public Car(int id) {
		this.id = id;
		rides = new ArrayList<>();
		free = true;
		rideWeightsAtTime = new TreeMap<>();
	}
	
	@Override
	public String toString() {
		int cnt = rides.size();
		String result = cnt + ""; 
		for (int i = 0; i < cnt; i++) {
			result += " " + rides.get(i).id;
		}
		return result;
	}
	public void setCarFree(boolean done){
		free = done;
	}


	public void addRide(Ride r) {
		long freeFrom = freeFromTime();
		r.realStartTime = freeFrom + Intersection.getDistance(currentIntersection(), r.getStartIntersection());
		r.realEndTime = freeFrom + Intersection.getDistance(currentIntersection(), r.getEndIntersection());
		rides.add(r);
	}
	
	public long freeFromTime() {
		int size = rides.size();
		return size == 0 ? 0 : rides.get(size-1).realEndTime;
	}
	
	public Intersection currentIntersection() {
		int nrides = rides.size();
		if (nrides == 0) {
			return new Intersection(0, 0);
		}
		else {
			return rides.get(nrides-1).getEndIntersection();
		}
	}
	
	public long getScoreForRide(Ride r, int bonus) {
		long score = 0;
		long totaldistance = Intersection.getDistance(currentIntersection(), r.getStartIntersection()) + Intersection.getDistance(r.getStartIntersection(), r.getEndIntersection());
		long freeFrom = freeFromTime(); 
		long endtime = freeFrom + totaldistance;
		if (endtime >= r.maxEndTime) {
			score = Long.MIN_VALUE;
		} else {
			score -= endtime;
			long arrivalAtStart = freeFrom + Intersection.getDistance(currentIntersection(), r.getStartIntersection());
			if (arrivalAtStart <= r.minStartTime) {
				score += bonus;
			}
		}
		return score;
	}
	public void cleanRideWeights(){
		rideWeightsAtTime.clear();
	}
	public void compareWeightsAtTime(int t, ArrayList<Ride> rides) {
		for(int r = 0; r < rides.size(); r++){
			long weight = Math.abs(rides.get(r).minStartTime - t + Intersection.getDistance(currentIntersection(), new Intersection(rides.get(r).startRow, rides.get(r).startCol)));
			rideWeightsAtTime.put(weight, rides.get(r));
		}
	}

	public Ride findZeroWeight() {
		long key = rideWeightsAtTime.firstKey();
		return rideWeightsAtTime.get(key);
	}

}
