import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;

public class Function1 {

	public static ArrayList<Ride> readData(BufferedReader br) throws NumberFormatException, IOException {
		String line;
		ArrayList<Ride> rL = new ArrayList<>();
		int id = 0;
		while ((line = br.readLine()) != null) {
			String[] bits = line.split(" ");
			int i = 0;
			rL.add(
				new Ride(id++,
						Integer.parseInt(bits[i++]), 
						Integer.parseInt(bits[i++]), 
						Integer.parseInt(bits[i++]), 
						Integer.parseInt(bits[i++]), 
						Long.parseLong(bits[i++]), 
						Long.parseLong(bits[i++]))
				);
		}
		return rL;
	}

	public static ArrayList<Car> createCars(int cars) {
		ArrayList<Car> cL = new ArrayList<>();
		for(int i = 0; i < cars; i++)
			cL.add(new Car(i));
		return cL;
	}

	public static ArrayList<Ride> sort(Worker worker) {
		worker.ridesList.sort(new Comparator<Ride>() {
			@Override
			public int compare(Ride o1, Ride o2) {
				return (int) (o1.minStartTime - o2.minStartTime);
			}
		});
		return worker.ridesList;
	}

}
