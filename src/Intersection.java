
public class Intersection {
	int row;
	int col;

	public Intersection(int r, int c) {
		row = r;
		col = c;
	}
	
	public static long getDistance(Intersection i1, Intersection i2) {
		return Math.abs(i1.row-i2.row)+Math.abs(i1.col-i2.col);
	}
}
