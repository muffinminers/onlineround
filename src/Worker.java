import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Worker {

	int rows;
	int cols;
	int cars;
	int rides;
	int bonus;
	long maxTime;

	ArrayList<Ride> ridesList;
	ArrayList<Car> carList;

	
	public static void main(String[] args) {
		Worker worker = new Worker();
		worker.run();
	}

	public void run() {
		System.out.println("I'm the workermain.");
		
		String[] files = new String[]{"a_example", "b_should_be_easy", "c_no_hurry", "d_metropolis", "e_high_bonus"};
		int counter = 0;
		while (counter < files.length) {
			String file = files[counter];
			try (FileReader fr = new FileReader("in" + File.separator + file + ".in");
				 BufferedReader br = new BufferedReader(fr);
				 FileWriter fw = new FileWriter("out" + File.separator + file + ".txt");
				 BufferedWriter bw = new BufferedWriter(fw);
				 PrintWriter pw = new PrintWriter(bw);) 
			{
				System.out.println("Parsing file: " + file);
				String[] bits = br.readLine().split(" ");
				rows = Integer.parseInt(bits[0]);
				cols = Integer.parseInt(bits[1]);
				cars = Integer.parseInt(bits[2]);
				rides = Integer.parseInt(bits[3]);
				bonus = Integer.parseInt(bits[4]);
				maxTime = Integer.parseInt(bits[5]);
				
				System.out.println(rows + "\t rows");
				System.out.println(cols + "\t cols");
				System.out.println(cars + "\t cars");
				System.out.println(rides + "\t rides");
				System.out.println(bonus + "\t bonus");
				System.out.println(maxTime + "\t maxtime");
				
				carList = Function1.createCars(cars);
				
				ridesList = Function1.readData(br);
				System.out.println(ridesList.size() + "\t in the rideslist");
				
				ridesList = Function1.sort(this);
				//Assign the rides
				Function2.assignRidesV3(this);
				
				
				Function3.writeResult(this, pw);
			} catch (Exception e) {
				e.printStackTrace();
			}
			counter++;
			//break;	
		}
		System.out.println("The end.");
	}
}
